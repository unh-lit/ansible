# Ansible

Ansible playbooks and ad-hoc commands for maintianing the UNH Digital Collections.  Also, instructions for set up.

See the [project documentation wiki](https://gitlab.com/unh-lit/hyrax516/-/wikis/home) for documentation.
